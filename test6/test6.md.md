﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010215102    姓名：代宇帆

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 创建用户并授权,连接数据库

``` sql
create user dyf identified by 123456;
grant connect, resource, dba to dyf;
grant unlimited tablespace  to dyf;
```

![image-20230525180253775](image-20230525180253775.png)

![image-20230525180523361](image-20230525180523361.png)

#### 创建表空间

```
CREATE TABLESPACE data_tbs
DATAFILE '/home/oracle/app/oracle/oradata/orcl/OracleSystem01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;

CREATE TABLESPACE index_tbs
DATAFILE '/home/oracle/app/oracle/oradata/orcl/MyOracleUserdata01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;
```

![image-20230525180719315](image-20230525180719315.png)

表空间结构：

| 表空间名称   | 表名称        | 表描述     |
| ------------ | :------------ | ---------- |
| data_tbs     | product       | 商品表     |
| data_tbs     | customer      | 客户表     |
| data_tbs     | order_details | 订单详情表 |
| data_tbs     | order_table   | 订单表     |
| index_tbs    | inventory     | 库存表     |



#### 创建相关表

1.商品表

```sql
CREATE TABLE product (
   product_id INT PRIMARY KEY,
   product_name VARCHAR(50),
   price NUMBER(10, 2),
   description VARCHAR(500)
)
TABLESPACE data_tbs;

```

![image-20230525180952550](image-20230525181049372.png) 

2.客户表

```sql
CREATE TABLE customer (
   customer_id INT PRIMARY KEY,
   customer_name VARCHAR(50),
   contact VARCHAR(20),
   address VARCHAR(100)
)
TABLESPACE data_tbs;
```

![image-20230525181114361](image-20230525181114361.png) 

3.订单详情表

```sql
-- 创建订单详情表
CREATE TABLE order_details (
   order_id INT,
   product_id INT,
   quantity INT,
   unit_price NUMBER(10, 2),
   PRIMARY KEY (order_id, product_id)
)
TABLESPACE data_tbs;

```

![image-20230525185424976](image-20230525185424976.png)

4.订单表

```sql
CREATE TABLE order_table (
   order_id INT PRIMARY KEY,
   customer_id INT,
   order_date DATE,
   order_status VARCHAR(20)
)
TABLESPACE data_tbs;

```

 ![image-20230525185440512](image-20230525185440512.png)

5.库存表

```sql
CREATE TABLE inventory (
   product_id INT PRIMARY KEY,
   quantity INT,
   last_updated DATE
)
TABLESPACE index_tbs;
```

 ![image-20230525185459472](image-20230525185459472.png)

6.索引

```sql
CREATE INDEX idx_customer_name ON customer (customer_name) TABLESPACE data_tbs;

CREATE INDEX idx_order_date ON order_table (order_date) TABLESPACE data_tbs;

CREATE INDEX idx_product_name ON product (product_name) TABLESPACE data_tbs;

```

以上代码分别创建了三个索引，其中两个用于加速查询，一个用于加速排序。

#### 插入填充数据

插入商品表

```sql
BEGIN
   FOR i IN 1..30000 LOOP
      INSERT INTO product (product_id, product_name, price, description) 
        VALUES (i, '商品' || i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2), '这是第' || i || '号商品');
   END LOOP;
   COMMIT;
END;
/


```

以上代码会向 product 表中插入 30000 条商品数据。其中，product_id 从 1 到 30000 依次递增，product_name 为 '商品' + 编号，price 为 10~1000 随机数（保留两位小数），description 为 '这是第' + 编号 + '号商品'。

![image-20230525184926833](image-20230525184926833.png) 



填充客户表

```sql
BEGIN
   FOR i IN 1..30000 LOOP
      INSERT INTO customer (customer_id, customer_name, contact, address) 
        VALUES (i, '客户' || i, '13888888888', '上海市浦东新区');
   END LOOP;
   COMMIT;
END;
/

```

以上代码会向 customer 表中插入 30000 条客户数据。其中，customer_id 从 1 到 30000 依次递增，customer_name 为 '客户' + 编号，contact 为一个固定的手机号码，address 为 '上海市浦东新区'。

![image-20230525185025720](image-20230525185025720.png) 

填充订单表

```sql
BEGIN
   FOR i IN 1..30000 LOOP
      INSERT INTO order_table (order_id, customer_id, order_date, order_status) 
        VALUES (i, ROUND(DBMS_RANDOM.VALUE(1,10000)), SYSDATE, '未付款');
   END LOOP;
   COMMIT;
END;
/
```

以上代码会向 order_table 表中插入 30000 条订单数据。其中，order_id 从 1 到 30000 依次递增，customer_id 为 1~10000 的随机数，order_date 为当前系统时间，order_status 为 '未付款'。

![image-20230525184631595](image-20230525184631595.png) 

填充订单详情表

```sql
BEGIN
   FOR i IN 1..30000 LOOP
      INSERT INTO order_details (order_id, product_id, quantity, unit_price) 
        VALUES (ROUND(DBMS_RANDOM.VALUE(1,30000)), ROUND(DBMS_RANDOM.VALUE(1,30000)), ROUND(DBMS_RANDOM.VALUE(1, 10)), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
   END LOOP;
   COMMIT;
END;
/
```

以上代码会向 order_details 表中插入 30000 条订单详情数据。其中，order_id 和 product_id 均为 1~30000 的随机数，quantity 为 1~10 的随机数，unit_price 为 10~100 的随机数（保留两位小数）。

![image-20230525184452869](image-20230525184452869.png)

填充库存详情表

```sql
BEGIN
   FOR i IN 1..30000 LOOP
      INSERT INTO inventory (product_id, quantity, last_updated) 
        VALUES (i, ROUND(DBMS_RANDOM.VALUE(50, 200)), SYSDATE);
   END LOOP;
   COMMIT;
END;
/
```

以上代码会向 inventory 表中插入 30000 条库存数据。其中，product_id 从 1 到 30000 依次递增，quantity 为 50~200 的随机数，last_updated 为当前系统时间。

![image-20230525184711646](image-20230525184711646.png) 

#### 设计权限及用户分配

```sql
创建管理员用户和授权：
CREATE USER admin IDENTIFIED BY admin_password;
GRANT CONNECT, RESOURCE, CREATE VIEW, CREATE PROCEDURE, CREATE SEQUENCE, CREATE TRIGGER, CREATE TABLE, CREATE SESSION TO admin;

创建普通用户和授权：
CREATE USER guest IDENTIFIED BY guest_password;
GRANT CONNECT, RESOURCE, CREATE SESSION TO guest;

```

 ![image-20230525184220976](image-20230525184220976.png)

#### PL/SQL设计

```sql
CREATE OR REPLACE PACKAGE my_package AS
  /* 存储过程1：根据客户ID查询其购买的商品数量 */
  PROCEDURE get_order_count_by_customer(p_customer_id IN NUMBER, p_count OUT NUMBER);

  /* 存储过程2：根据商品ID查询其销售额 */
  PROCEDURE get_sales_by_product(p_product_id IN NUMBER, p_sales OUT NUMBER);

  /* 函数1：根据订单ID计算订单总金额 */
  FUNCTION get_total_amount(p_order_id IN NUMBER) RETURN NUMBER;

  /* 函数2：根据客户ID查询其购买的所有商品名称 */
  FUNCTION get_product_names_by_customer(p_customer_id IN NUMBER) RETURN VARCHAR2;
END my_package;
/

CREATE OR REPLACE PACKAGE BODY my_package AS
  /* 存储过程1：根据客户ID查询其购买的商品数量 */
  PROCEDURE get_order_count_by_customer(p_customer_id IN NUMBER, p_count OUT NUMBER) AS
  BEGIN
    SELECT SUM(quantity)
    INTO p_count
    FROM order_details od
    JOIN order_table ot ON od.order_id = ot.order_id
    WHERE ot.customer_id = p_customer_id;
  END;

  /* 存储过程2：根据商品ID查询其销售额 */
  PROCEDURE get_sales_by_product(p_product_id IN NUMBER, p_sales OUT NUMBER) AS
  BEGIN
    SELECT SUM(quantity * unit_price)
    INTO p_sales
    FROM order_details
    WHERE product_id = p_product_id;
  END;

  /* 函数1：根据订单ID计算订单总金额 */
  FUNCTION get_total_amount(p_order_id IN NUMBER) RETURN NUMBER AS
    v_total_amount NUMBER := 0;
  BEGIN
    SELECT SUM(quantity * unit_price)
    INTO v_total_amount
    FROM order_details
    WHERE order_id = p_order_id;

    RETURN v_total_amount;
  END;

  /* 函数2：根据客户ID查询其购买的所有商品名称 */
  FUNCTION get_product_names_by_customer(p_customer_id IN NUMBER) RETURN VARCHAR2 AS
    v_product_names VARCHAR2(1000) := '';
    v_product_name VARCHAR2(50);
  BEGIN
    FOR c IN (
      SELECT DISTINCT p.product_name
      FROM order_details od
      JOIN order_table ot ON od.order_id = ot.order_id
      JOIN product p ON od.product_id = p.product_id
      WHERE ot.customer_id = p_customer_id
    ) LOOP
      v_product_name := c.product_name || ',';
      v_product_names := v_product_names || v_product_name;
    END LOOP;

    RETURN v_product_names;
  END;
END my_package;
/


```

  ![image-20230525183842205](image-20230525183842205.png)

   



#### 备份方案

建立备份策略：确定备份周期、备份介质、备份保留期等参数，例如周一至周五每天增量备份，周六进行全量备份，备份数据存储在磁盘上，保留备份结果最近7天的数据。

创建RMAN备份脚本：使用RMAN（Recovery Manager）工具进行备份，编写备份脚本。以下是一种备份脚本示例：

run {
 allocate channel t1 type disk;
 backup incremental level 1 for recover of copy with tag 'mydb-tag' database include current controlfile;
 delete noprompt obsolete;
}
解释如下：

level 1 表示增量备份的级别为 1。
for recover of copy with tag 'mydb-tag' 表示备份用于恢复指定的标签 backed up in a previous backup set。
database include current controlfile 表示备份包含整个数据库及当前控制文件。
delete noprompt obsolete 表示删除过时的备份。
执行以上脚本即可进行增量备份。

创建全量备份脚本：在备份周期到达时，采用相同的方式创建全量备份脚本执行备份。

建立自动化脚本：使用Shell脚本或定时任务等方式，将备份脚本进行定时执行。以下是一个Shell脚本示例：

#!/bin/bash
export ORACLE_HOME=/u01/app/oracle/product/11.2.0/dbhome_1
export PATH=$ORACLE_HOME/bin:$PATH

rman target / catalog rman_backup/rman_password <<EOF
 run {
   allocate channel t1 type disk;
   backup incremental level 1 for recover of copy with tag 'mydb-tag' database include current controlfile;
   delete noprompt obsolete;
 }
EOF

验证备份：在脚本执行完成后，可手动验证备份结果是否符合预期。

### 总结
在我看来，基于Oracle数据库的商品销售系统设计实现是一项非常有价值的实验。在实验中，我们需要熟悉Oracle数据库的基本操作和SQL语言，同时还需要掌握Java等编程语言的开发技能，以及对于系统架构和设计思路的理解。下面是我的一些实验心得：

数据库设计很重要：数据库是应用程序的核心，为了保证系统的效率、可靠性和扩展性，需要对数据库进行良好的规划和设计。例如，数据表的字段类型、索引、约束等都需要仔细考虑，以满足不同业务场景下的需求。

系统架构应具备灵活性：在实验中，我们需要考虑不同业务场景下的系统需求，因此系统架构应该具备一定的灵活性。例如，可以采用Spring框架来实现系统模块化，以便随时添加或删除不同的功能模块。

代码的可维护性：在编写代码时，需要充分考虑代码可读性和可维护性。因为在实际业务中，系统需要不断进行迭代和优化，因此代码易读易懂易修改是非常重要的。

编写单元测试：编写单元测试可以有效保证系统的质量和稳定性。我们可以采用JUnit等测试框架来编写测试用例，并在代码提交之前运行测试用例进行测试。

学会使用第三方库和工具：现代开发不再是孤立的操作，通过使用优秀第三方库和工具可以提高生产效率及质量。例如，使用MyBatis等ORM框架可以简化数据库操作，使用Log4j等日志库可以方便记录系统日志。

总体而言，这个基于Oracle数据库的商品销售系统设计实验，涉及到了许多技术和知识，需要将这些知识融会贯通才能更好的完成实验。同时，还需要不断学习和尝试新技术，以适应不断变化的技术环境。

 

